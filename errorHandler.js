export default function paymentErrorHandler(error) {
  let message = error.message || 'Unknown error'

  // if it is api error
  if (error.data && error.data.message) {
    message = error.data.message
  }

  // if it is http error
  if (error.response && error.response.data) {
    message = error.response.data.message
  }

  return Promise.reject({ message })
}
