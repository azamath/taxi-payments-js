import errorHandler from './errorHandler'

export default function payWithCard(intent, card_id, stripe) {
  return stripe
    .confirmCardPayment(intent.client_secret, {
      payment_method: card_id,
    })
    .then(result => {
      if (result.error) {
        return Promise.reject({ message: result.error.message })
      } else {
        return result
      }
    })
    .catch(errorHandler)
}
