import Vue from 'vue'
import errorHandler from './errorHandler'

export default function payWithBancontact(source) {
  return new Promise((resolve, reject) => {
    const width = 420
    const height = 500
    const left = screen.width ? (screen.width - width) / 2 : 0
    const top = screen.height ? (screen.height - height) / 2 : 0
    const popup = window.open(
      source.redirect.url,
      'bancontactPopup',
      `height=${height},width=${width},left=${left},top=${top},` +
        `resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes`,
    )

    const closeAndClear = () => {
      clearInterval(closeChecker)
      popup && popup.close()
      window.removeEventListener('message', messageListener)
    }

    const closeChecker = setInterval(() => {
      if (!popup || popup.closed) {
        closeAndClear()
        resolve()
      }
    }, 100)

    const messageListener = ({ data }) => {
      // if message data doesn't contain Stripe source properties
      // probably it is not our message
      if (!data.object || !data.id || !data.status) {
        return
      }

      if (data.status === 'failed') {
        closeAndClear()
        reject({
          message: Vue.$t('orders.messages.payment_failed'),
        })
      } else if (data.status === 'cancelled') {
        closeAndClear()
        reject({
          message: Vue.$t('orders.messages.payment_cancelled'),
        })
      } else {
        closeAndClear()
        resolve()
      }
    }

    // opener window will send message via postMessage after redirect
    // the messageListener will analyse message data and resolve a Promise
    window.addEventListener('message', messageListener)
  }).catch(errorHandler)
}
